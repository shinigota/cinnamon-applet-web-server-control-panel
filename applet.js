const Applet = imports.ui.applet;
const PopupMenu = imports.ui.popupMenu;
const Gettext = imports.gettext.domain('cinnamon-extensions');
const _ = Gettext.gettext;
const Util = imports.misc.util;
const Lang = imports.lang; 
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const St = imports.gi.St;
const AppletMeta = imports.ui.appletManager.applets['web-server-control-panel@benjamin-barbe.fr'];
const AppletDir = imports.ui.appletManager.appletMeta['web-server-control-panel@benjamin-barbe.fr'].path;

var Properties = {
	APPLET_TITLE : "Web server - Control panel",
	COMMANDS     : {
        TOGGLE_SERVICE       : "gksu systemctl -g %COMMAND %SERVICE" 
	},
    ACTIONS      : {
        HTTPD_CONFIGURATION  : { LABEL: "Open Apache configuration",    COMMAND: "xfce4-terminal -e='vim /etc/httpd/conf/httpd.conf'" },
        HTTPD_FOLDER         : { LABEL: "Open Apache folder",           COMMAND: "xdg-open /etc/httpd" },
        HTTPD_LOGS           : { LABEL: "Open Apache logs",             COMMAND: "xdg-open /var/log/httpd" },
        MYSQLD_CONFIGURATION : { LABEL: "Open MySQL configuration",     COMMAND: "xdg-open /etc/mysql/my.cnf" },
        MYSQLD_FOLDER        : { LABEL: "Open MySQL folder",            COMMAND: "xdg-open /etc/httpd" },
        MYSQLD_LOGS          : { LABEL: "Open MySQL logs",              COMMAND: "" },
        PHP_CONFIGURATION    : { LABEL: "Open PHP configuration",       COMMAND: "xfce4-terminal -e='vim /usr/share/webapps/phpMyAdmin/config.inc.php'" },
        PHP_FOLDER           : { LABEL: "Open PHP folder",              COMMAND: "" },
        PHP_LOGS             : { LABEL: "Open PHP logs",                COMMAND: "" },
    },
    SWITCHES     : {
        HTTPD                : { ID: "enable-httpd",   LABEL: "Apache",  SERVICE: "httpd",  COMMANDS: { START:"gksu systemctl start httpd.service", STOP: "gksu systemctl stop httpd.service"} },
        MYSQLD               : { ID: "enable-mysqld",  LABEL: "MySQL",   SERVICE: "mysqld", COMMANDS: { START:"gksu systemctl start mysqld",        STOP: "gksu systemctl stop mysqld"} },
    },
	ICON_PATH    : AppletDir + "/icon.svg",
};

var switches = {};

function MyApplet(orientation) {
    this._init(orientation);
}

MyApplet.prototype = {
    __proto__: Applet.IconApplet.prototype,

    _init: function(orientation) {
        Applet.IconApplet.prototype._init.call(this, orientation);

        this.set_applet_icon_path(Properties.ICON_PATH);
        this.set_applet_tooltip(_(Properties.APPLET_TITLE));

        this.menuManager = new PopupMenu.PopupMenuManager(this);
        this.menu        = new Applet.AppletPopupMenu(this, orientation);
        this.menuManager.addMenu(this.menu);

        this.createSwitchItem(Properties.SWITCHES.HTTPD);
        this.createAction(Properties.ACTIONS.HTTPD_CONFIGURATION);
        this.createAction(Properties.ACTIONS.HTTPD_LOGS);
        this.createAction(Properties.ACTIONS.HTTPD_FOLDER);

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this.createSwitchItem(Properties.SWITCHES.MYSQLD);
        this.createAction(Properties.ACTIONS.MYSQLD_CONFIGURATION);
        this.createAction(Properties.ACTIONS.MYSQLD_LOGS);
        this.createAction(Properties.ACTIONS.MYSQLD_FOLDER);

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this.createAction(Properties.ACTIONS.PHP_CONFIGURATION);
        this.createAction(Properties.ACTIONS.PHP_LOGS);
        this.createAction(Properties.ACTIONS.PHP_FOLDER);
    },

    createSwitchItem: function(switchProperty) {
        var isServiceActive = this.isServiceActive(switchProperty.SERVICE);
        var switchItem = new PopupMenu.PopupSwitchMenuItem(_(switchProperty.LABEL), isServiceActive);
        switchItem.key = switchProperty.ID;
        switchItem.connect('toggled', Lang.bind(this, this.onSwitchPressed));
        this.menu.addMenuItem(switchItem);
        switches[switchProperty.ID] = {switch: switchItem, property: switchProperty};
    },

    createAction: function(action) {
        this.menu.addAction(_(action.LABEL), function(event) {
            Util.spawnCommandLine(action.COMMAND);
        });
    },


    onSwitchPressed: function(item) {
        switch (item.key) {
            case Properties.SWITCHES.HTTPD.ID:
                this.toggleSetting(item.state, Properties.SWITCHES.HTTPD);
                break;
            case Properties.SWITCHES.MYSQLD.ID:
                this.toggleSetting(item.state, Properties.SWITCHES.MYSQLD);
                break;  
            default:
        }
     },

    toggleSetting: function(itemState, switchProperty) {
        Util.spawnCommandLine(generateToggleCommand(itemState, switchProperty.SERVICE));
    },

    generateToggleCommand: function(serviceState, serviceName) {
        return Properties.COMMANDS.TOGGLE_SERVICE.replace("%COMMAND", serviceState ? "start" : "stop").replace("%SERVICE", serviceName);
    },

    isServiceActive: function(service) {
        s=GLib.spawn_async_with_pipes(null, ["pgrep",service], null, GLib.SpawnFlags.SEARCH_PATH,null)
        c=GLib.IOChannel.unix_new(s[3])
               
        let [res, pid, in_fd, out_fd, err_fd] =
          GLib.spawn_async_with_pipes(
            null, ["pgrep",service], null, GLib.SpawnFlags.SEARCH_PATH, null);
        out_reader = new Gio.DataInputStream({ base_stream: new Gio.UnixInputStream({fd: out_fd}) });
               
        let [out, size] = out_reader.read_line(null);

        return out != null;
    },

    on_applet_clicked: function(){
        this.menu.toggle();
        for (var key in switches) {
            var switchValue = switches[key];
            var isServiceActive = this.isServiceActive(switchValue.property.SERVICE);
            switchValue.switch.setToggleState(isServiceActive);
        };
    },
};

function main(metadata, orientation, panel_height, instance_id) {
    return new MyApplet(orientation, panel_height, instance_id);
}